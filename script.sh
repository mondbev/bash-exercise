#!/usr/bin/env bash

main(){

#     Gets 2 positional arguments: <name> <count> and any number of additional arguments

if [ $# -gt 2 ]; then
     posName=$1
     posCount=$2
    shift 2
     additionalArgs=$@
else echo "Please init the script as ./script.sh <name> <count> and any number of additional arguments"
fi

#     Checks that all additional arguments are non-numeric strings (fails if not)

for arg in $additionalArgs; do
    [[ $arg =~ ^[0-9]+$ ]] && { echo "additional arguments must be non-numeric strings" && exit 1; }
done

#     Creates a directory $HOME/mylogs

[[ ! -d $HOME/mylogs ]] && mkdir $HOME/mylogs

#     Creates a file called <name>.log in the directory

touch $HOME/mylogs/$posName.log

#     For <count> times iterates over the list of additional arguments
#         And writes a line with a the argument string and a random word (you can use echo $(($RANDOM%7)) for bash) out of the following list : 

randomWordArr=("normal" "request" "failure" "notice" "warning" "alert" "error" "note")
for (( i=0; i < posCount; i++)); do
	echo "${additionalArgs[*]} ${randomWordArr[$(($RANDOM%8))]}" >> $HOME/mylogs/$posName.log
done

}

main "$@"