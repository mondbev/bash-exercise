#!/usr/bin/env bash

zipAndClean(){
  logState=$1 #healty, unhealty..
  shift
  arrayOfLogs=($@)
  [[ ${#arrayOfLogs[@]} -gt 0 ]] && for f in ${arrayOfLogs[*]}; do mv $HOME/mylogs/$f $HOME/mylogs/$logState/; done && \
                                  tar -C $HOME/mylogs -cvf $logState.tar $logState && \
                                  mv $logState.tar $HOME/mylogs/$logState && \
                                  rm $HOME/mylogs/$logState/*.log 
}
# main script was skipped
[[ ! -d $HOME/mylogs ]] && { echo "please run script.sh before this script" && exit 1; }

# main script was not skipped
if ls $HOME/mylogs/*.log 1> /dev/null 2>&1
then
# create dirs to hold log files if not exists
for d in unhealthy unstable healthy
do
[[ ! -d $HOME/mylogs/$d ]] && mkdir $HOME/mylogs/$d
done
arrUnhealthy=()
arrUnstable=()
arrHealthy=()
    for f in $HOME/mylogs/*.log
    do
# start calculations
       noOfLines=$(cat $f | wc -l)
       noOfError=$(cat $f | grep -c "error")
       noOfFailure=$(cat $f | grep -c "failure")
       noOfWarning=$(cat $f | grep -c "warning")
       noOfAlert=$(cat $f | grep -c "alert")
       errPrctng=$(echo $noOfError*100/$noOfLines|bc -l)
       failPrctng=$(echo $noOfFailure*100/$noOfLines|bc -l)
       warningPrctng=$(echo $noOfWarning*100/$noOfLines|bc -l)
       alertPrctng=$(echo $noOfAlert*100/$noOfLines|bc -l)
# If lines with ‘error’ and ‘failure’ constitute >10% of all lines = log is unhealthy       
       if (( $(echo "$failPrctng+$errPrctng > 10" |bc -l) )) 
       then arrUnhealthy+=("${f##*/}")
       else 
      # If lines with ‘error’ and ‘failure’ are <10% but lines with ‘warning’ and ‘alert’ are > 20% = log is unstable
        if (( $(echo "$warningPrctng+$alertPrctng > 20" |bc -l) ))
        then arrUnstable+=("${f##*/}")
      # All the rest of the logs are considered healthy
        else arrHealthy+=("${f##*/}")
        fi
       fi
     done
     # zip files and remove the remains.
     zipAndClean "unhealthy" "${arrUnhealthy[*]}"
     zipAndClean "unstable" "${arrUnstable[*]}"
     zipAndClean "healthy" "${arrHealthy[*]}"
    # Print coloured output
    RED='\033[0;31m'
    ORANGE='\033[0;33m'
    GREEN='\033[0;32m'
    printf "${GREEN}healthy | ${#arrHealthy[@]} log files | ${arrHealthy[*]}\n"
    printf "${ORANGE}unstable | ${#arrUnstable[@]} log files | ${arrUnstable[*]}\n"
    printf "${RED}unhealthy | ${#arrUnhealthy[@]} log files | ${arrUnhealthy[*]}\n"
else
# main script was skipped
    echo "please run script.sh before this script" && exit 1;
fi
