script.sh:

    Gets 2 positional arguments: <name> <count> and any number of additional arguments

    Checks that all additional arguments are non-numeric strings (fails if not)

    Creates a directory $HOME/mylogs

    Creates a file called <name>.log in the directory

    For <count> times iterates over the list of additional arguments
        And writes a line with a the argument string and a random word (you can use echo $(($RANDOM%7)) for bash) out of the following list :

           - normal
           - request 
           - failure
           - error
           - warning
           - alert
           - notice
           - note
    
log_processor.sh:

        Scans each file in $HOME/mylogs directory

        For each file: 

           - If lines with ‘error’ and ‘failure’ constitute >10% of all lines = log is unhealthy

           - If lines with ‘error’ and ‘failure’ are <10% but lines with ‘warning’ and ‘alert’ are > 20% = log is unstable

           - All the rest of the logs are considered healthy

        Zip the logs and copy them accordingly to subfolders: 

           - $HOME/mylogs/unhealthy

           - $HOME/mylogs/unstable

           - $HOME/mylogs/healthy

        Print out a report colouring the output accordingly, e.g:

GREEN: healthy | 5 log files | ab.log, course.log, devops.log, good.log, fine.log

ORANGE: unstable | 2 log files | ocean.log, earthquake.log

RED: unhealthy | 3 log files | random.log, 23.log, balbala.log